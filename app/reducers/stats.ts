import { createEntityAdapter, createSlice } from '@reduxjs/toolkit';

import { Store } from '../store';

type Stat = { id: string; value: number };

const statsAdapter = createEntityAdapter<Stat>();

const statsSlice = createSlice({
  name: 'stats',
  initialState: statsAdapter.getInitialState(),
  reducers: {
    updateOne: statsAdapter.updateOne,
    addOne: statsAdapter.addOne,
    upsertOne: statsAdapter.upsertOne,
  },
});

const { updateOne, addOne, upsertOne } = statsSlice.actions;

export const selectors = statsAdapter.getSelectors();

export default statsSlice.reducer;
export function gain(store: Store, id: string, delta: number) {
  const sel = statsAdapter.getSelectors();
  const state = store.getState().stats;
  const stat = sel.selectById(state, id);
  if (stat != undefined) {
    store.dispatch(updateOne({ id, changes: { value: stat.value + delta } }));
  } else {
    store.dispatch(addOne({ id, value: delta }));
  }
}

export function set(store: Store, id: string, value: number) {
  store.dispatch(upsertOne({ id, value }));
}
