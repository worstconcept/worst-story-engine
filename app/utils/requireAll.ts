export default function requireAll(r) {
  return Object.fromEntries(
    r.keys().map(function (mpath, ...args) {
      const result = r(mpath, ...args);
      const name = mpath
        .replace(/(?:^[.\/]*\/|\.[^.]+$)/g, ''); // Trim
//        .replace(/\//g, '_'); // Relace '/'s by '_'s
      return [name, result];
    })
  );
}
