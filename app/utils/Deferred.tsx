import React from 'react';
// License CC0 1.0: https://creativecommons.org/publicdomain/zero/1.0/

export default class Deferred extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }

  componentDidMount() {
    this.props.promise.then((value) => {
      this.setState({ value });
    });
  }

  render() {
    const then = this.props.then || ((value) => <span>{value}</span>);
    return then(this.state.value);
  }
}
