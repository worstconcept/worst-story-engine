import React from 'react';
import JSXParser from 'react-jsx-parser';
import { useStore } from 'react-redux';
import Deferred from '../utils/Deferred';
import { selectors } from '../reducers/stats';
import { useParams } from 'react-router-dom';

export default function StoryLetComponent(props): JSX.Element {
  const params = useParams();
  const store = useStore();
  const {stats} = store.getState();
  const stats_list_fat = selectors.selectEntities(stats);
  const stats_list = {};
  for (let k in stats_list_fat) {
    stats_list[k] = stats_list_fat[k].value;
  }
  stats_list['__']=stats_list;
  return (
    <div className="StoryLet" id={`let-${params.id}`} data-tid="container">
      <h2 className="text"><JSXParser bindings={{_in:'h2'}} jsx={props.text} /></h2>
      {props.longtext ? (
        <JSXParser className="longtext" bindings={stats_list} jsx={props.longtext} />
      ) : (
        ''
      )}
      {props.result
        ? props.result.map((result) => {
            const key = result[0];
            const op = result[1];
            const val = result[2];
            const datapromise = import(
              /* webpackMode: "lazy" */ 'Game/stats/' + key + '.yaml'
            );
            return (
              <Deferred key={key} promise={datapromise} then={ data => data.hidden?'':
                (<div className={`result ${op=='='?"set":(op=='+'?"inc":"dec")}`} id={`res-${key}`}>{val} {data.name}</div>)
              } />
            );
          })
        : ''}
    </div>
  );
}
