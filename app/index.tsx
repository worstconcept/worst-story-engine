import React, { Fragment } from 'react';
import { render } from 'react-dom';
import { AppContainer as ReactHotAppContainer } from 'react-hot-loader';
import { history, configuredStore } from './store';
import 'fontsource-roboto';
// import { AccessAlarm, ThreeDRotation } from '@material-ui/icons';
import './app.global.css';

import GameCfg from 'Game/config.yaml';

const store = configuredStore();

const AppContainer = process.env.PLAIN_HMR ? Fragment : ReactHotAppContainer;

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line global-require
  const Root = require('./containers/Root').default;
  document.title = GameCfg.title || 'Untitled WorstStoryEngine Game';
  try{
    require("Game/game.global.scss");
  }catch(err){
    try{
      require("Game/game.global.sass");
    }catch(err){
      try{
        require("Game/game.global.css");
      }catch(err){};
    };
  };
  render(
    <AppContainer>
      <Root store={store} history={history} />
    </AppContainer>,
    document.getElementById('root')
  );
});
