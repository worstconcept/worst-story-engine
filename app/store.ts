import { configureStore, getDefaultMiddleware, Action } from '@reduxjs/toolkit';
import { createHashHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import { createLogger } from 'redux-logger';
import { ThunkAction } from 'redux-thunk';
import { save, load } from 'redux-localstorage-simple';
import GameCfg from 'Game/config.yaml';

// eslint-disable-next-line import/no-cycle
import createRootReducer from './reducers/root';

export const history = createHashHistory();
const rootReducer = createRootReducer(history);
export type RootState = ReturnType<typeof rootReducer>;

const router = routerMiddleware(history);
const saver = save({
  ignoreStates: ['router'],
  namespace: GameCfg.title,
  namespaceSeparator: "//",
  debounce: 500,
});
const middleware = [...getDefaultMiddleware(), router, saver];

const excludeLoggerEnvs = ['test', 'production'];
const shouldIncludeLogger = !excludeLoggerEnvs.includes(
  process.env.NODE_ENV || ''
);

if (shouldIncludeLogger) {
  const logger = createLogger({
    level: 'info',
    collapsed: true,
  });
  middleware.push(logger);
}

export const configuredStore = (initialState?: RootState) => {
  const loaded = load({
    preloadedState: initialState,
    namespace: GameCfg.title,
    namespaceSeparator: "//",
  });
  // Create Store
  const store = configureStore({
    reducer: rootReducer,
    middleware,
    preloadedState: loaded,
  });

  if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept(
      './reducers/root',
      // eslint-disable-next-line global-require
      () => store.replaceReducer(require('./reducers/root').default)
    );
  }
  return store;
};
export type Store = ReturnType<typeof configuredStore>;
export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;
