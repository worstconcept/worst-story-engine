/* eslint react/jsx-props-no-spreading: off */
import React from 'react';
import { Switch, Route, Redirect, useParams } from 'react-router-dom';
import GameCfg from 'Game/config.yaml';
import routes from './constants/routes.json';
import App from './containers/App';
import ActionContainer from './containers/Action';
import StoryLetContainer from './containers/StoryLet';

export default function Routes() {
  return (
    <App>
      <Switch>
        <Route path={`${routes.STORYLET}:id+`} component={StoryLetContainer} />
        <Route path={`${routes.ACTION}:id+`} component={ActionContainer} />
        <Route path="/">
          <Redirect to={routes.ACTION + GameCfg.homelet} />
        </Route>
      </Switch>
    </App>
  );
}
