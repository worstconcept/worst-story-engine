import React from 'react';
import { useParams, Redirect } from 'react-router-dom';
import { useStore } from 'react-redux';

import routes from '../constants/routes.json';

import { gain, set as setto } from '../reducers/stats';
import { push } from 'connected-react-router'

export default function ActionContainer() {
  const params = useParams();
  const store = useStore();
  const datapromise = import(
    /* webpackMode: "lazy" */ `Game/storylets/${params.id}.yaml`
  );
  datapromise.then((data) => {
    if (data.result)
      for (const result of data.result) {
        const key = result[0];
        let val = result[2];
        switch (result[1]) {
          case '=':
            setto(store, key, val);
            break;
          case '-':
            val = 0 - val;
          case '+':
            gain(store, key, val);
        }
      }
    store.dispatch(push(routes.STORYLET + params.id))
  });
  return <></>;
}
