import React from 'react';
import { useStore } from 'react-redux';
import JSXParser from 'react-jsx-parser';

import { Link as RouterLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { selectors } from '../reducers/stats';

import requireAll from '../utils/requireAll';

import routes from '../constants/routes.json';

export default function NextActionsContainer() {
  const allModules = requireAll(
    require.context(
      // Any kind of variables cannot be used here
      'Game/storylets/', // (Webpack based) path
      true, // Use subdirectories
      /\.yaml$/ // File name pattern
    )
  );
  const available = [];
  const store = useStore();
  const { stats } = store.getState();
  for (const key in allModules) {
    let ok = true;
    if(allModules[key].prereqs)
      for (const test of allModules[key].prereqs) {
        const stat = selectors.selectById(stats, test[0]);
        let stillok = false;
        if (test[1] == '<' && (stat == undefined || stat.value < test[2])) {
          stillok = true;
        }
        if (test[1] == '>' && stat != undefined && stat.value > test[2]) {
          stillok = true;
        }
        if (test[1] == '=' && stat != undefined && stat.value == test[2]) {
          stillok = true;
        }
        if (!stillok) {
          ok = false;
          break;
        }
      }
    if (ok) available.push(key);
  }
  return (
    <div className="NextActionsContainer">
      {available.map((key) => (
        <Button id={`act-${key}`} key={key} component={RouterLink} to={routes.ACTION + key}>
          <JSXParser bindings={{_in:'button'}} jsx={allModules[key].text} />
        </Button>
      ))}
    </div>
  );
}
