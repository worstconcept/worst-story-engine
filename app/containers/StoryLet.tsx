import React from 'react';
import { useParams } from 'react-router-dom';

import Deferred from 'utils/Deferred';

import StoryLetComponent from '../components/StoryLet';
import GameCfg from 'Game/config.yaml';

import NextActionsContainer from './NextActions';
import JSXParser from 'react-jsx-parser';
import { useStore } from 'react-redux';
import { selectors } from 'reducers/stats';

export default function StoryLetContainer() {
  const params = useParams();
  const store = useStore();
  const {stats} = store.getState();
  const stats_list_fat = selectors.selectEntities(stats);
  const stats_list = {};
  for (let k in stats_list_fat) {
    stats_list[k] = stats_list_fat[k].value;
  }
  stats_list['__']=stats_list;
  const datapromise = import(
    /* webpackMode: "lazy" */ `Game/storylets/${params.id}.yaml`
  );
  return (
    <>
      {
        GameCfg.header?(
          <div id='header'>
            <JSXParser bindings={stats_list} jsx={GameCfg.header} />
          </div>
        ): ''
      }
      <div className="StoryLetContainer">
        <Deferred
          promise={datapromise}
          then={(data) => <StoryLetComponent {...data} />}
        />
        <NextActionsContainer />
      </div>
      {
        GameCfg.footer?(
          <div id='footer'>
            <JSXParser bindings={stats_list} jsx={GameCfg.footer} />
          </div>
        ): ''
      }
    </>
  );
}
