# What's this about?

Kinda like StoryNexus, just less dead.

# Setup

* clone/fork the repo
* run `yarn` to set up

# Making your game

* set `title`, `homelet` in `game/config.yaml`
* optionally write a `game/game.global.{sc,sa,c}ss`
* write storylets and stats in `game/{storylets,stats}` (if you use subfolders, those are part of their internal names!)
* run `yarn dev` for a realtime hot reloading developer mode version of your game
* run `yarn package-all` to build your game for release, or one of the following platform specific:
    - `yarn package-linux` (various linux packages)
    - `yarn package-linux-AI` (just an AppImage)
    - `yarn package-mac`
    - `yarn package-win`

# Using as a submodule

The engine checks for the existence of `../config.yaml`, and in that case uses `..` instead of `game` as its game folder. This means you can use it as a submodule in your game's repo.


# Reference

## `config.yaml`

```yaml
# the title of your game
title: Your Game

# the storylet to load upon starting
homelet: the_init_storylet

# OPTIONAL shows above the storylets
header: some jsx

# OPTIONAL shows below the storylets
footer: some jsx
```

both `header` and `footer` can refer to stats as variables (e.g. `{some_stat}`) (and they're part of a `__` object, to deal with special chars such as `/`)

## `storylets/**.yaml`

```yaml
# shows in buttons and storylet title
text: short jsx

# shows as the storylet main text
longtext: long jsx

# OPTIONAL only makes this storylet available if all are valid
prereqs:
    - ['some_stat','=',3] # only if some_stat is 3
    - ['some_stat','<',3] # only if it's < 3 or you don't have it
    - ['some_stat','>',3] # only if it's > 3

# OPTIONAL changes to stats when selecting the storylet
result:
    - ['some_stat','=',3] # sets it to 3
    - ['some_stat','+',3] # increments it by 3
    - ['some_stat','-',3] # decrements it by 3
```

`text` can refer to `_in` (either `h2` or `button`) to see where it's displayed (e.g. `{_in=="h2"?"You're there!":"Go there!"}`, `longtext` can refer to stats like described above.

## `stats/**.yaml`

```yaml
# OPTIONAL this makes it so changes to it are not listed
hidden: true

# OPTIONAL what it's shown as when listed
name: some html
```

the filename (or path, everything between `stats/` and `.yaml` to be precise) is what you'll use to refer to it in prereqs/results or jsx.

to allow for subfolders when referring from jsx, use `{__['some_folder/some_stat']}` because of the `/` not being a valid char in variables. when not dealing with a subfolder, `{some_stat}` will suffice.

## `game.global.{sc,sa,c}ss`

```scss
#header {
    // the header
}

#footer {
    // the footer
}

.StoryLetContainer {
    .StoryLet {
        &#let-some_storylet {
            // only applies to some_storylet
        }

        .text {
            // the text
        }

        .longtext {
            // the longtext
        }

        .result {
            // each result

            &#res-some_stat {
                // only applies to some_stat
            }

            &.set {
                // if using =
            }

            &.inc {
                // if using +
            }

            &.dec {
                // if using -
            }
        }
    }

    .NextActionsContainer {
        // all buttons to available storylets

        a[role="button"] {
            // each button

            &#act-some_storylet {
                // only applies to buttons to some_storylet
            }
        }
    }
}
```

just use the devtools to figure out more specific stuff, but that's the important ones.
